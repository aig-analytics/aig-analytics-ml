WISHLIST_HISTORY_QUERY = \
    """

    with games_info as(
    
        select
            cast(sai.appid as int),
            sai.popular_tags,
            sai.release_date,
            sail.price
    
        from steam_app_info as sai
        left join steam_app_info_lkp as sail on sai.appid = sail.appid
        where sai.release_date is not null and release_date != 'UNK'
    ),
    
    max_steam_members_games as (
    
        select
            appid,
            player_id,
            max(last_played) as max_last_played
        from steam_members_games
        group by
        appid, player_id
    ),
    
    agg_steam_members_games as (
            select
            smg.appid,
            smg.player_id,
            msmg.max_last_played,
            smg.hours,
            smg.hours_forever
        from max_steam_members_games as msmg
            left join steam_members_games as smg
            on msmg.appid=smg.appid and msmg.player_id=smg.player_id 
            and msmg.max_last_played = smg.last_played
    )
    select
           smw.*,
           gi.popular_tags,
           gi.release_date,
           gi.price,
           asmg.hours,
           asmg.hours_forever,
           asmg.max_last_played
    
    from steam_members_wishlist as smw
        left join games_info as gi on smw.appid = gi.appid
        left join agg_steam_members_games as asmg on smw.appid = asmg.appid 
        and smw.player_id = asmg.player_id;
    """

GLOBAL_WISHLISTS_QUERY = \
    """
    select * from steam_top_whishlist
    """

BESTSELLERS_QUERY = \
    """
    select * from steam_top_bestsellers
    """

STEAM_GAME_TIME_STATS_QUERY = \
    """
    select * from steam_game_time_stats
    """
