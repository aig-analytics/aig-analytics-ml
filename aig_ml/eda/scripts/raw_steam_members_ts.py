import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import tensorflow as tf
import tqdm

# sys.path.append('/home/kw/Projects/aig-analytics/aig-analytics-ml')

from aig_ml.datamart_connection import DataMartConnection
from aig_ml.db_interfaces import PGDataInterface
from aig_ml.eda.queries import WISHLIST_HISTORY_QUERY, GLOBAL_WISHLISTS_QUERY, \
    BESTSELLERS_QUERY, STEAM_GAME_TIME_STATS_QUERY
from aig_ml.eda.utils import get_data, sort_and_cumsum

if __name__ == '__main__':

    dbname = 'gamesanalytics'
    dbhost = '40.87.97.193'
    dbuser = 'kw'
    dbpass = 'kw12#'
    schema = 'datamart'

    dc = \
        DataMartConnection(
            dbname=dbname, user=dbuser, host=dbhost, pwd=dbpass, schema=schema)
    eng = dc.get_engine()

    pgdi = PGDataInterface(pg_engine=eng)

    filenames = \
        ['wishlists.csv', 'top_wishlists.csv', 'bestsellers.csv',
         'steam_game_time_stats.csv']

    # load data from disk
    data_sources = {fn.split('.')[0]: fn for fn in filenames}
    loaded_data = get_data(
        pg_data_interface=pgdi, data_sources=data_sources, download_data=False)

    wishlists = loaded_data['wishlists']
    top_wishlists = loaded_data['top_wishlists']
    bestsellers = loaded_data['bestsellers']
    steam_game_time_stats = loaded_data['steam_game_time_stats']

    last_seen_on_wishlist_cols = ['appid', 'created_date']

    top_wishlists['created_date'] = \
        top_wishlists['created_date'].apply(pd.to_datetime)

    # print(top_wishlists.head().to_string())
    # print(steam_game_time_stats.head().to_string())
    # input('check')

    # #### START ####

    last_seen_on_wishlist = \
        top_wishlists.groupby(last_seen_on_wishlist_cols[0])\
        .apply(lambda x:
               pd.DataFrame(columns=last_seen_on_wishlist_cols) if pd.isnull(x.name)
               else pd.DataFrame(
                       [[x.name, x[last_seen_on_wishlist_cols[1]].max()]],
                       columns=last_seen_on_wishlist_cols)) \
        .reset_index(drop=True)

    releases_on_top_wishlist = \
        last_seen_on_wishlist[last_seen_on_wishlist_cols[0]].unique()

    first_seen_on_wishlist = \
        top_wishlists.groupby(last_seen_on_wishlist_cols[0]) \
        .apply(lambda x:
               pd.DataFrame(
                   columns=last_seen_on_wishlist_cols) if pd.isnull(x.name)
               else pd.DataFrame(
                   [[x.name, x[last_seen_on_wishlist_cols[1]].min()]],
                   columns=last_seen_on_wishlist_cols)) \
        .reset_index(drop=True)

    last_seen_in_time_stats = \
        steam_game_time_stats.groupby(last_seen_on_wishlist_cols[0]) \
        .apply(lambda x:
               pd.DataFrame(columns=last_seen_on_wishlist_cols) if pd.isnull(x.name)
               else pd.DataFrame(
                   [[x.name, x[last_seen_on_wishlist_cols[1]].max()]],
                   columns=last_seen_on_wishlist_cols)) \
        .reset_index(drop=True)

    first_seen_in_time_stats = \
        steam_game_time_stats.groupby(last_seen_on_wishlist_cols[0]) \
        .apply(lambda x:
               pd.DataFrame(columns=last_seen_on_wishlist_cols) if pd.isnull(x.name)
               else pd.DataFrame(
                   [[x.name, x[last_seen_on_wishlist_cols[1]].min()]],
                   columns=last_seen_on_wishlist_cols)) \
        .reset_index(drop=True)

    grouping_cols = ['appid', 'player_id', 'added']
    created_date_col = 'created_date'

    max_create_dates = \
        wishlists[grouping_cols + [created_date_col]] \
        .groupby(grouping_cols) \
        .agg('max').reset_index()
    max_create_dates.columns = grouping_cols + ['max_created_date']

    wishlists_with_max_dates = \
        pd.merge(
            left=wishlists, right=max_create_dates, left_on=grouping_cols,
            right_on=grouping_cols, how='left').reset_index(drop=True)

    wishlist_unique_adds = \
        wishlists_with_max_dates[
            wishlists_with_max_dates['created_date'] ==
            wishlists_with_max_dates['max_created_date']]

    # print(wishlists.head())
    wishlist_unique_adds['added_date'] = \
        wishlist_unique_adds['added'].apply(pd.to_datetime).apply(
            lambda x: x.date())

    wishlists_ts = \
        wishlist_unique_adds.groupby(['appid', 'added_date']).apply(
            lambda x: len(x)) \
        .reset_index()

    wishlists_ts.columns = ['appid', 'added_date', 'added_count']

    adds_per_game = \
        wishlists_ts.groupby(['appid']) \
        .apply(lambda x: x['added_count'].sum()).reset_index()

    adds_per_game.columns = ['appid', 'adds_per_game']

    cusum_kwargs = {
        'sorting_column': 'added_date',
        'cumsumed_column': 'added_count',
        'cumsum_column': 'added_cumsum'
    }

    wishlists_cumsum_ts = \
        wishlists_ts[wishlists_ts['appid'].isin(
            last_seen_in_time_stats['appid'].unique())]\
        .groupby('appid').apply(sort_and_cumsum, **cusum_kwargs)\
        .reset_index(drop=True)

    wishlists_cumsum_ts_wide = \
        pd.pivot(data=wishlists_cumsum_ts, index='added_date', columns='appid',
                 values=['added_cumsum', 'added_count'])

    wishlists_cumsum_ts_wide.columns = \
        wishlists_cumsum_ts_wide.columns = \
        ['_'.join([str(int(cn) if isinstance(cn, float) else cn) for cn in
                   col]).strip()
         for col in wishlists_cumsum_ts_wide.columns.values]

    wishlists_cumsum_ts_wide = \
        wishlists_cumsum_ts_wide.reset_index() \
        .fillna(method='ffill').fillna(0)

    wishlists_cumsum_ts_wide['added_date'] = \
        wishlists_cumsum_ts_wide['added_date'].apply(pd.to_datetime)

    # wishlists_cumsum_ts_wide.to_csv(
    #     '/home/kw/Projects/aig-analytics/aig-analytics-ml/data/sanity_check/'
    #     'pivoted_cumsum_wishlists.csv', index=False)
    #
    # input('sanity check')

    # #### END ####

    # pivot steam members
    dropped_columns = ['created_dy', 'last_modified_date', 'last_modified_by']
    pivoted_time_stats = \
        steam_game_time_stats[
            steam_game_time_stats['appid'].isin(releases_on_top_wishlist)]\
        .drop(columns=dropped_columns)\
        .sort_values(['appid', 'created_date'], ascending=True)\
        .reset_index(drop=True)

    # pivoted_time_stats.to_csv(
    #     '/home/kw/Projects/aig-analytics/aig-analytics-ml/data/sanity_check/'
    #     '2b_pivoted_steam_time_stats.csv', index=False)

    time_stats_wide = \
        pd.pivot(data=pivoted_time_stats, index='created_date', columns='appid')\
        .fillna(method='ffill').fillna(0)\
        .reset_index()

    time_stats_wide.columns = \
        time_stats_wide.columns = \
        ['_'.join([str(int(cn) if isinstance(cn, float) else cn) for cn in
                   col]).strip()
         for col in time_stats_wide.columns.values]

    time_stats_wide.rename(
        columns={'created_date_': 'dt'}, inplace=True)

    time_stats_wide['dt'] = time_stats_wide['dt'].apply(pd.to_datetime)

    wishlists_droped_columns = \
        ['id', 'itemkey', 'href', 'title', 'release_date', 'price'] + \
        dropped_columns

    pivoted_top_wishlists = \
        top_wishlists[~pd.isnull(top_wishlists['appid'])].drop(columns=wishlists_droped_columns)\
        .sort_values(['appid', 'created_date'], ascending=True)\
        .reset_index(drop=True)

    top_wishlists_wide = \
        pd.pivot(data=pivoted_top_wishlists, index='created_date', columns='appid') \
        .fillna(method='ffill').fillna(0) \
        .reset_index()

    top_wishlists_wide.columns = \
        top_wishlists_wide.columns = \
        ['_'.join([str(int(cn) if isinstance(cn, float) else cn) for cn in
                   col]).strip()
         for col in top_wishlists_wide.columns.values]

    top_wishlists_wide.rename(
        columns={'created_date_': 'dt'}, inplace=True)

    forecast_horizon_days = 30

    # pd.Timedelta(forecast_horizon_days, unit='days')

    all_dates = \
        pd.date_range(
            top_wishlists_wide['dt'].min(),
            top_wishlists_wide['dt'].max() -
            pd.Timedelta(forecast_horizon_days, unit='days'),
            freq=pd.Timedelta(1, unit='d')) \
        .to_series() \
        .apply(lambda x: x.date()) \
        .reset_index(drop=True)

    X_dt = pd.DataFrame({'dt': all_dates, })
    X_dt['dt'] = X_dt['dt'].apply(pd.to_datetime)

    X_time_stats = \
        pd.merge(left=X_dt, left_on='dt', right=time_stats_wide, right_on=['dt'])

    X_top_wishlists = \
        pd.merge(
            left=X_time_stats, left_on='dt', right=top_wishlists_wide,
            right_on=['dt']).fillna(method='ffill')

    X_cumsum_wishlists = \
        pd.merge(
            left=X_time_stats, left_on='dt', right=wishlists_cumsum_ts_wide,
            right_on=['added_date']).fillna(method='ffill')
