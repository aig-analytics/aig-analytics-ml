import numpy as np
import os
import tensorflow as tf
import pandas as pd

from aig_ml.constants import DEFAULT_SAVE_PATH_PATTERN



def sort_and_cumsum(
        in_df: pd.DataFrame, sorting_column: str, cumsumed_column: str,
        cumsum_column: str, min_wishlist_adds: int = 20):
    sorted_df = in_df.sort_values(sorting_column).reset_index(drop=True)
    sorted_df[cumsum_column] = sorted_df[cumsumed_column].cumsum()

    if sorted_df[cumsum_column].max() < min_wishlist_adds:
        return pd.DataFrame()

    return sorted_df


def min_max_transform_np_array(processed_array: np.ndarray) -> np.ndarray:
    min_from_input = np.min(processed_array)
    max_from_input = np.max(processed_array)

    min_max_denom = max_from_input - min_from_input

    result = (processed_array - min_from_input) / min_max_denom
    return result


def min_max_transform_cumsum_and_trend(x: np.ndarray):

    assert x.shape[1] == 2

    scaled_x = np.empty(x.shape)
    scaled_x[:, 0] = \
        min_max_transform_np_array(x[:, 0] - x[0, 0])
    scaled_x[:, 1] = min_max_transform_np_array(x[:, 1])

    return scaled_x


def get_data(
        pg_data_interface, data_sources: dict, download_data: bool = False,
        save_path_pattern: str = DEFAULT_SAVE_PATH_PATTERN):

    loaded_data = {}

    if download_data:

        for table_name, query in data_sources.items():
            loaded_data[table_name] = pg_data_interface.get_df(query=query)

    else:

        for table_name, csv_filename in data_sources.items():
            loaded_data[table_name] = \
                pd.read_csv(save_path_pattern.format(csv_filename))

    return loaded_data
