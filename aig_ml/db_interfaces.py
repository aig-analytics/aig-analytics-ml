import pandas as pd
from sqlalchemy.engine import Engine

from aig_ml.datamart_connection import DataMartConnection


class PGDataInterface:

    @property
    def pg_engine(self):
        return self._pg_engine

    @pg_engine.setter
    def pg_engine(self, value) -> Engine:
        self._pg_engine = value

    def __init__(self, pg_engine: Engine):
        self.pg_engine = pg_engine

    def exec_query(self, query: str):
        with self.pg_engine.connect() as conn:
            result = conn.execute(query)

        return result

    def get_df(self, table_name: str = None, query: str = None):

        if not table_name and not query:
            raise ValueError(
                'Both parameters: `table_name` and `query` are empty')

        if table_name:
            read_sql_str = table_name
        else:
            read_sql_str = query

        result = pd.read_sql(read_sql_str, con=self.pg_engine)
        return result

    def put_df(self):
        pass

    def update_table(self):
        pass


if __name__ == '__main__':

    dbname = 'gamesanalytics'
    dbhost = '40.87.97.193'
    dbuser = 'kw'
    dbpass = 'kw12#'
    schema = 'datamart'

    dc = \
        DataMartConnection(
            dbname=dbname, user=dbuser, host=dbhost, pwd=dbpass, schema=schema)
    eng = dc.get_engine()

    pgdi = PGDataInterface(pg_engine=eng)
    table_name = 'steam_members_reviews'
    q = 'select * from steam_members_reviews'
    rev_df = pgdi.get_df(query=q)
    print(rev_df.head().to_string())
