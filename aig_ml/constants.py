import os
from pathlib import Path


PACKAGE_ROOT = Path(os.sep.join(str(__file__).split(os.sep)[:-2]))

DEFAULT_SAVE_PATH_PATTERN = str(PACKAGE_ROOT / 'data' / 'input') + os.sep + '{}'
