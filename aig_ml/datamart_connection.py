import psycopg2

from sqlalchemy import create_engine


class DataMartConnection:

    @property
    def dbname(self) -> str:
        return self._dbname

    @dbname.setter
    def dbname(self, new_val):
        self._dbname = new_val

    @property
    def user(self) -> str:
        return self._user

    @user.setter
    def user(self, new_val):
        self._user = new_val

    @property
    def host(self) -> str:
        return self._host

    @host.setter
    def host(self, new_val) -> str:
        self._host = new_val

    @property
    def port(self) -> str:
        return self._port

    @port.setter
    def port(self, new_val):
        self._port = new_val

    @property
    def pwd(self):
        return self._pass

    @pwd.setter
    def pwd(self, value):
        self._pass = value

    @property
    def schema(self):
        return self._schema

    @schema.setter
    def schema(self, value):
        self._schema = value

    def __init__(
            self, dbname: str, user: str, host: str, pwd: str,
            schema: str, port: str = '5432'):
        self.dbname = dbname
        self.user = user
        self.host = host
        self.pwd = pwd
        self.port = port
        self.schema = schema

    def create_connection(self):
        return psycopg2.connect(
                dbname=self.dbname,
                user=self.user,
                host=self.host,
                password=self.pwd,
                port=self.port,
                options=f'-c search_path={self.schema}')
        # return psycopg2.connect(
        #     dbname=self.dbname, user=self.user, host=self.host,
        #     password=self._pass, port=self.port)

    def get_engine(self):

        return create_engine(
            'postgresql+psycopg2://',
            creator=lambda x: self.create_connection())

        # return create_engine(
        #     'postgresql://{0}:{1}@{2}:{3}/{4}'.format(
        #         self._user, self._pass, self.host, self.port,
        #         self.dbname))
