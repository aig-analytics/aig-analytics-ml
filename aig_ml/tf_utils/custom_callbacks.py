import tensorflow as tf


class Every20thEpochReporter(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        if epoch % 20 == 0:
            train_loss = logs['loss']
            valid_loss = logs['val_loss']

            print(
                'Epoch: {} | loss: {} | valid loss: {}'
                .format(epoch, train_loss, valid_loss))
