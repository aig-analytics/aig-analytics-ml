from setuptools import setup, find_packages


if __name__ == '__main__':
    setup(
        name='aig_ml',
        version='0.1',
        description='ML lib for games preds',
        author='aig-analytics',
        packages=find_packages()
    )
